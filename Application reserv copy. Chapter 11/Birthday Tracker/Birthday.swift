//
//  Birthday.swift
//  Birthday Tracker
//
//  Created by Михаил on 07/02/2019.
//  Copyright © 2019 3M Software. All rights reserved.
//

import Foundation

class Birthday {
    
    let firstName: String
    let lastName: String
    let birthdate: Date
    
    init(firstName: String, lastName: String, birthdate: Date) {
        
        self.firstName = firstName
        self.lastName = lastName
        self.birthdate = birthdate
    }
}
